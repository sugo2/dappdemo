import type { InjectedAccountWithMeta } from '@polkadot/extension-inject/types';
import { encodeAddress } from '@polkadot/util-crypto';

interface Props {
    account: InjectedAccountWithMeta
    prefix: number
    name: string
}

const Address: React.FC<Props> = ({ account, prefix, name }) => {
    // Return the balance if it exists
    return (
        <div>
            <p className='text-sm'>{name} Address {encodeAddress(account.address, prefix)}</p>
        </div>
    )

};

export default Address