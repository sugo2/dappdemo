import { ApiPromise } from '@polkadot/api';
import type { InjectedAccountWithMeta, InjectedExtension } from '@polkadot/extension-inject/types';

interface Props {
    api: ApiPromise | undefined
    account: InjectedAccountWithMeta
    extension: InjectedExtension | undefined
}

export const CallNoteImage: React.FC<Props> = ({ api, account, extension }: Props) => {

    async function callNoteImageExtrinsic() {
        if (!api || !account || !account.address) return;

        // FIXME: This works only if we are connected to the Polimec Testnet since the polimecFunding is a Polimec specific pallet that is on Polimec!
        // HOWTOSOLVE: Connect to wss://polimec.rocks (when fly.io is up and running)

        // Option 1: Sign and send the transaction in the browser
        api.tx.polimecFunding
            .noteImage("This should be the hash of the project's metadata")
            .signAndSend(account.address, { signer: extension?.signer }, ({ status, dispatchError }) => {
                // status would still be set, but in the case of error we can shortcut
                // to just check it (so an error would indicate InBlock or Finalized)
                if (dispatchError) {
                    if (dispatchError.isModule) {
                        // for module errors, we have the section indexed, lookup
                        const decoded = api.registry.findMetaError(dispatchError.asModule);
                        const { docs, name, section } = decoded;

                        console.log(`${section}.${name}: ${docs.join(' ')}`);
                    } else {
                        // Other, CannotLookup, BadOrigin, no extra info
                        console.log(dispatchError.toString());
                    }
                }
                // TODO: Use toast instead setStatus(status.type);
            });

        // Option 2: Sign the transaction in the browser and send it to Cipolla, which will send it
        // const extrinsicCall = api.tx.polimecFunding.noteImage("This should be the hash of the project's metadata");
        // const signedCall = await extrinsicCall.signAsync(account.address, { signer: extension?.signer });
        // TODO: Add a GraphQL Mutation to pass the `signedCall` to Cipolla
    }

    return (
        <>
            {/* <p>Call the extrinsic `noteImage()` from the `polimecFunding` pallet</p>
            <button type="button" onClick={() => callNoteImageExtrinsic()}>Note</button> */}
        </>
    );
}