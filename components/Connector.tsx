import { useExtension, useApi } from 'useink';
import { CurrentBlock } from './CurrentBlock';
import { Balance } from './Balance';
import Address from './Address';
import { Transfer } from './Transfer';
import { CallNoteImage } from './CallNoteImage';
import { AccountSelector } from './AccountSelector';

// const POLIMEC_ADDRESS_ON_KILT = '5FHneW46xGXgs5mUiveU4sbTyGBzmstUspZC92UhjJM694ty';

const Connector = () => {
    const { accounts, account, connect, disconnect, extension, setAccount, } = useExtension();
    const { api } = useApi();

    const DECIMALS = api?.registry.chainDecimals[0] || 12;
    const UNIT = api?.registry.chainTokens[0] || 'PLMC';
    const SS58_FORMAT = api?.registry.chainSS58 || 42;
    const CURRENT_CHAIN_NAME = api?.runtimeChain.toHuman() || 'Polimec Testnet';

    if (!accounts || accounts.length === 0 || !account) {
        return (
            <div className="flex justify-center items-center h-screen">
                <div className="flex justify-center items-center bg-poison-green p-12 rounded-lg drop-shadow-lg">
                    <button className="rounded-lg bg-white p-2 drop-shadow-lg" onClick={connect}>Connect the Wallet</button>
                </div>
            </div>
        )
    }

    // Else return the disconnect button if the account is connected and the Balance component
    return (
        <div className="flex justify-center items-center h-screen space-y-4" >
            <div className="space-y-2 mx-auto w-full max-w-lg bg-poison-green rounded-lg drop-shadow-xl p-8">
                <div className='flex flex-col space-y-4'>
                    <AccountSelector account={account} accounts={accounts} setAccount={setAccount} />
                    <Address account={account} prefix={SS58_FORMAT} name={CURRENT_CHAIN_NAME} />
                </div>
                <Balance account={account} decimals={DECIMALS} withUnit={UNIT} />
                <CurrentBlock />
                <Transfer account={account} api={api} decimals={DECIMALS} extension={extension} withUnit={UNIT} />
                <CallNoteImage account={account} api={api} extension={extension} />
                <br />
                <button className="rounded-lg bg-white p-2 drop-shadow-lg " onClick={disconnect}>Disconnect the Wallet</button>
            </div>
        </div>
    );

};

export default Connector;