import { formatBalance } from '@polkadot/util';
import type { InjectedAccountWithMeta } from '@polkadot/extension-inject/types';
import { useBalance } from "useink"

interface Props {
    account: InjectedAccountWithMeta
    decimals: number
    withUnit: string
}

export const Balance: React.FC<Props> = ({ account, decimals, withUnit }) => {
    const balance = useBalance(account);
    if (!balance) return null;

    const freeBalance = formatBalance(balance?.freeBalance, { decimals, withUnit });

    return <p>Free Balance: {freeBalance}</p>
}