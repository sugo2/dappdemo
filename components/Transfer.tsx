import { useState } from "react";
import { isValidAddress } from '../functions/validate';
import { ApiPromise } from '@polkadot/api';
import type { InjectedAccountWithMeta, InjectedExtension } from '@polkadot/extension-inject/types';
import toast from "react-hot-toast";
import { ProcessingButton } from "./ProcessingButton";

interface Props {
    withUnit: string
    decimals: number
    api: ApiPromise | undefined
    account: InjectedAccountWithMeta
    extension: InjectedExtension | undefined
    // email: string TODO: Get it from the Credential
}

export const Transfer: React.FC<Props> = ({ withUnit, api, account, extension, decimals }: Props) => {
    const [amount, setAmount] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(false);
    const [destAddress, setDestAddress] = useState<string | null>(null);

    async function transfer(amount: bigint) {
        if (!api || !account || !account.address || !destAddress) return;
        if (!isValidAddress(destAddress)) {
            toast.error('Invalid Destination Address');
            return
        }

        /// Option 1: Sign and send the transaction in the browser

        api.tx.balances
            .transfer(destAddress, amount)
            .signAndSend(account.address, { signer: extension?.signer }, ({ status, dispatchError, txHash}) => {
                setLoading(true)
                // status would still be set, but in the case of error we can shortcut
                // to just check it (so an error would indicate InBlock or Finalized)
                if (dispatchError) {
                    if (dispatchError.isModule) {
                        // for module errors, we have the section indexed, lookup
                        const decoded = api.registry.findMetaError(dispatchError.asModule);
                        const { docs, name, section } = decoded;
                        toast.error(`${section}.${name}: ${docs.join(' ')}`)
                        setLoading(false)
                    } else {
                        // Other, CannotLookup, BadOrigin, no extra info
                        toast.error(dispatchError.toString())
                        setLoading(false)
                    }
                }
                if (status.isInBlock) {
                    // TODO: Add a GraphQL Mutation to save stuff in the database
                    console.log(`Transaction included at blockHash ${txHash}`);
                    toast.success(`${status.type} @ blockHash ${txHash}`)
                }
                if (status.isFinalized) {
                    // TODO: Add a GraphQL Mutation to save stuff in the database
                    console.log(`Transaction finalized at blockHash ${txHash}`);
                    toast.success(`${status.type} @ blockHash ${txHash}`)
                    setLoading(false)
                }
            });

        /// TODO: Check if with Capi on the Backend we can send the transaction and send the status updates
        /// Option 2: Sign the transaction in the browser and send it to Cipolla, which will send it
        
        // const transfer = api.tx.balances.transfer(destAddress, amount);
        // try {
        //     const signedTransfer = await transfer.signAsync(account.address, { signer: extension?.signer });
        //     const JSONTransfer = signedTransfer.toJSON();
        //     // const send = await api.tx.call(arrayofBytes, arrayofBytes).send();
        //     console.log(JSONTransfer);
        //     // TODO: Add a GraphQL Mutation to pass the `signedTransfer` to Cipolla
        // } catch (e: any) {
        //     toast.error(e.message);
        //     return;
        // }
    }

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        transfer(toBalance(amount));
    }

    function toBalance(amount: number): bigint {
        return BigInt(amount * 10 ** decimals);
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-6">
                <label htmlFor="amount" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Amount</label>
                <input required id="amount" type="number" min={0} step={1 * (10) ** -decimals} onChange={(e) => setAmount(parseFloat(e.target.value))} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg  block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" />
            </div>
            <div className="mb-6">
                <label htmlFor="address" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Destination Address</label>
                <input required type="text" id="address" onChange={(e) => setDestAddress(e.target.value)} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg  block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" />
            </div>
            {amount === 0 && <></>}
            {amount > 0 && loading && <div className="flex justify-end">
                <ProcessingButton />
            </div>}
            {amount > 0 && !loading && <div className="flex justify-end">
                <button type="submit" className="text-white bg-dark-green hover:drop-shadow-lg focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center">
                    Transfer {amount} {withUnit}
                </button>
            </div>}
        </form>
    );
}