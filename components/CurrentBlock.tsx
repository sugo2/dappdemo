import { useBlockHeader } from "useink";

export const CurrentBlock: React.FC = () => {
    const block = useBlockHeader();

    return (
        <>
            <b>Current Block: </b>
            <span>
                {block?.blockNumber === undefined ? '--' : block.blockNumber}
            </span>
        </>
    );
}