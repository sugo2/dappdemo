import Head from 'next/head'
import dynamic from 'next/dynamic';

const Connector = dynamic(() => import('../../components/Connector'), {
  loading: () => <p>Loading...</p>,
  ssr: false,
});

export default function Home() {
  return (
    <>
      <Head>
        <title>Polimec</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Connector />
    </>
  )
}
