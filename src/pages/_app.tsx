import dynamic from 'next/dynamic';
import type { AppProps } from 'next/app'
import { InkConfig } from 'useink';
import { Inter } from 'next/font/google'
import '../../styles/global.css'
import { Toaster } from 'react-hot-toast';
import { ApolloClient, InMemoryCache, ApolloProvider, gql } from '@apollo/client';


const inter = Inter({ subsets: ['latin'] })

// REMARK: This is a workaround to avoid SSR errors (e.g. "window is not defined")
const UseInkProvider: React.ComponentType<React.PropsWithChildren<InkConfig>> = dynamic(
  () => import('useink').then(({ UseInkProvider }) => UseInkProvider),
  { ssr: false },
);

function App({ Component, pageProps }: AppProps) {
  const client = new ApolloClient({
    uri: 'http://localhost:3000/graphql',
    cache: new InMemoryCache(),
  });
  return (
    <UseInkProvider
      config={{
        dappName: 'Polimec',
        providerUrl: "wss://polimec.rocks",
      }}
    >
      <ApolloProvider client={client}>
        <main className={inter.className}>
          <Toaster />
          <Component {...pageProps} />
        </main>
      </ApolloProvider>
    </UseInkProvider>
  );
}

export default App
